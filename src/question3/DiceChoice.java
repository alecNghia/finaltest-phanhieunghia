package question3;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;

public class DiceChoice implements EventHandler<ActionEvent>{
	private TextField bet;
	private TextField msg;
	private String action;
	private Label bal;
	private DiceGame d;
	
	public DiceChoice (String action, TextField bet, TextField msg, Label bal, DiceGame d) {
		this.action = action;
		this.bet = bet;
		this.msg = msg;
		this.bal = bal;
		this.d = d;
	}

	@Override
	public void handle(ActionEvent arg) {
		msg.setText(d.rollDice(action));
		bet.setText(Integer.toString(d.getBet()));
		bal.setText(Integer.toString(d.getBal()));
	}
}
