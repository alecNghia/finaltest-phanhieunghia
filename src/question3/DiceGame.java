//Phan Hieu Nghia 1834104

package question3;

import java.util.*;

public class DiceGame {
	private int bal = 250;
	private int bet;
	
	public int getBal() {
		return this.bal;
	}
	
	public int getBet() {
		return this.bet;
	}
	
	//How the game will work
	public String rollDice(String playerChoice) {
		Random rand = new Random();
		int max = 6;
		int min = 1;
		int randChoice = rand.nextInt((max - min) + 1) + min;

		String msg = "";
		String pcChoice;
		
		if(randChoice == 1 || randChoice == 3 || randChoice == 5) {
			pcChoice = "Odd";
		} else {
			pcChoice = "Even";
		}
		
		
		if(bet < 0 && bet > bal) {
			System.err.println("Your input is invalid");
		} else if (bet > 0 && bet <= bal && playerChoice.equalsIgnoreCase("Odd") && pcChoice.equalsIgnoreCase("Odd")) {
			bal = bal + bet;
			msg = "You have won and doubled your money";
		} else if (bet > 0 && bet <= bal && playerChoice.equalsIgnoreCase("Even") && pcChoice.equalsIgnoreCase("Even")){
			bal = bal + bet;
			msg = "You have won and doubled your money";
		} else {
			msg = "You have lost, lel!";
		}
		return playerChoice;
	}
}
