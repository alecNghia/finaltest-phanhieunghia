package question3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class DiceApp extends Application{
	private DiceGame d = new DiceGame();

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Group root = new Group();
		Scene scene = new Scene(root, 400, 400);
		scene.setFill(Color.WHITE);
		
		VBox overall = new VBox();
		HBox buttons = new HBox();
		HBox bet = new HBox();
		HBox result = new HBox();
		
		//Buttons 
		Button even = new Button("Even");
		Button odd = new Button("Odd");
		buttons.getChildren().addAll(even, odd);
		
		//Label and textfield
		Label availCash = new Label("Balance: " + d.getBal());
		
		Text betText = new Text("You bet:");
		TextField betAmount = new TextField();
		
		Text resultText = new Text("The result:");
		TextField resultTf = new TextField();
				
		
		bet.getChildren().addAll(betText, betAmount);
		bet.setSpacing(5);
		bet.setPrefWidth(100);
		
		result.getChildren().addAll(resultText, resultTf);
		result.setSpacing(5);
		result.setPrefWidth(100);
		
		overall.getChildren().addAll(buttons, availCash, bet, result);
		overall.setSpacing(5);
		root.getChildren().add(overall);
		primaryStage.setTitle("Welcome dice game");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
