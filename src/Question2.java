//Phan Hieu Nghia 1834104	

public class Question2 {
	public static void main(String[] args) {
		int[] test = {2,3,4,5,6};
		int n = 0;
		System.out.println(recursiveCount(test, n));
	}
	//Question 2
		public static int recursiveCount(int[] numbers, int n) {
			if(n < numbers.length) {
				//Smaller than 10, located at even index, and greater or equal to n
				if(numbers[n] < 10 && (numbers[n] % 2 == 0) && numbers[n] >= n) {
					return recursiveCount(numbers, n+1)+1;
				}
				return recursiveCount(numbers, n+1);
			}
			return 0;
		}
}
