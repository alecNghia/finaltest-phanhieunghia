//Phan Hieu Nghia 1834104

public class Question1 {
	public static void main(String[] args) {
		foo(5);
	}
	
	//Question 1
	public static int foo(int n) {
        if (n <= 0) {
               return Math.abs(n);
        }
        
        System.out.println("N:" + n);
		int x = foo(n-2);
        System.out.println("X: " + x);

        int y = foo(n-1);
        System.out.println("Y: " + y);
        int z = foo(n-1);
        
        System.out.println("Z:" + z);
        int sum = x + y + z;          
        System.out.println("Sum:" + sum);
        
        return sum;
 }
}

